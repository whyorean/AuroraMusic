/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.music;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.aesthetic.Aesthetic;
import com.afollestad.materialdialogs.color.ColorChooserDialog;
import com.percolate.caffeine.ViewUtils;

public class ThemesActivityFragment extends Fragment {
    View rootView;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.app_theme_inc, container, false);
        ViewUtils.findViewById(getActivity(),R.id.layout).setVisibility(View.GONE);

        setupSelection();
        setupTheme();
        setupReload();
        setupReset();
        return rootView;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
    }

    @Override
    public void onSaveInstanceState(Bundle outcicle) {
        super.onSaveInstanceState(outcicle);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ViewUtils.findViewById(getActivity(),R.id.layout).setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private ColorChooserDialog chooserPrimary() {
        return new ColorChooserDialog.Builder(getActivity(), R.string.themes)
                .titleSub(R.string.action_themes)
                .accentMode(false)
                .doneButton(R.string.md_done_label)
                .cancelButton(R.string.md_cancel_label)
                .backButton(R.string.md_back_label)
                .dynamicButtonColor(true)
                .show((FragmentActivity) getActivity());
    }

    private ColorChooserDialog chooserAccent() {
        return new ColorChooserDialog.Builder(getActivity(), R.string.themes)
                .titleSub(R.string.action_themes)
                .accentMode(true)
                .doneButton(R.string.md_done_label)
                .cancelButton(R.string.md_cancel_label)
                .backButton(R.string.md_back_label)
                .dynamicButtonColor(true)
                .show((FragmentActivity) getActivity());
    }

    private void setupReset() {
        TextView themeReset = (TextView) rootView.findViewById(R.id.themeReset);
        themeReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Aesthetic.get()
                        .colorPrimaryRes(R.color.colorPrimary)
                        .colorAccentRes(R.color.colorAccent)
                        .colorStatusBarAuto()
                        .colorNavigationBarAuto()
                        .apply();
            }
        });
    }

    private void setupReload() {
        TextView themeRestart = (TextView) rootView.findViewById(R.id.themeRestart);
        themeRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = getActivity().getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getActivity().getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }

    private void setupSelection() {
        TextView primarySelect = (TextView) rootView.findViewById(R.id.selectPrimary);
        TextView primaryAccent = (TextView) rootView.findViewById(R.id.selectAccent);

        primarySelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooserPrimary();
            }
        });

        primaryAccent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooserAccent();
            }
        });
    }

    private void setupTheme() {
        TextView themeLight = (TextView) rootView.findViewById(R.id.themeLight);
        TextView themeDark = (TextView) rootView.findViewById(R.id.themeDark);
        TextView themeBlack = (TextView) rootView.findViewById(R.id.themeBlack);

        themeLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Aesthetic.get()
                        .activityTheme(R.style.AppTheme)
                        .isDark(false)
                        .colorWindowBackgroundRes(R.color.colorBackground)
                        .colorCardViewBackgroundRes(R.color.colorBackgroundCard)
                        .colorNavigationBarAuto()
                        .textColorPrimaryRes(R.color.colorTextPrimary)
                        .textColorSecondaryRes(R.color.colorTextSecondary)
                        .apply();
            }
        });

        themeDark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Aesthetic.get()
                        .activityTheme(R.style.AppTheme_Dark)
                        .isDark(true)
                        .colorWindowBackgroundRes(R.color.colorBackgroundDark)
                        .colorCardViewBackgroundRes(R.color.colorBackgroundCardDark)
                        .textColorPrimaryRes(R.color.colorTextPrimaryDark)
                        .textColorSecondaryRes(R.color.colorTextSecondaryDark)
                        .apply();
            }
        });

        themeBlack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Aesthetic.get()
                        .activityTheme(R.style.AppTheme_Dark)
                        .isDark(true)
                        .colorWindowBackgroundRes(R.color.colorBackgroundBlack)
                        .colorCardViewBackgroundRes(R.color.colorBackgroundCardBlack)
                        .textColorPrimaryRes(R.color.colorTextPrimaryDark)
                        .textColorSecondaryRes(R.color.colorTextSecondaryDark)
                        .apply();
            }
        });
    }
}
