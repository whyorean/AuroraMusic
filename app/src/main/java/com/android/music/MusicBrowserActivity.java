/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.music;

import android.Manifest.permission;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.aesthetic.Aesthetic;
import com.afollestad.aesthetic.NavigationViewMode;
import com.afollestad.materialdialogs.color.ColorChooserDialog;
import com.android.music.MusicUtils.ServiceToken;
import com.codeaurora.music.custom.FragmentsFactory;
import com.codeaurora.music.custom.MusicPanelLayout.BoardState;
import com.codeaurora.music.custom.MusicPanelLayout.ViewHookSlipListener;
import com.codeaurora.music.custom.PermissionActivity;

import java.util.Timer;
import java.util.TimerTask;

import me.tankery.lib.circularseekbar.CircularSeekBar;

public class MusicBrowserActivity extends MediaPlaybackActivity implements MusicUtils.Defs,
        NavigationView.OnNavigationItemSelectedListener, ColorChooserDialog.ColorCallback {

    private static final String TAG = "MusicBrowserActivity";
    private static final String[] REQUIRED_PERMISSIONS = {
            permission.READ_PHONE_STATE,
            permission.READ_EXTERNAL_STORAGE,
            permission.WRITE_EXTERNAL_STORAGE};

    private static final long RECENTLY_ADDED_PLAYLIST = -1;
    private static final long ALL_SONGS_PLAYLIST = -2;
    private static final long PODCASTS_PLAYLIST = -3;
    private static final long DEFAULT_PLAYLIST = -5;

    public static boolean mIsParentActivityFinishing;

    String mArtistID, mAlbumID;
    String mIntentAction;
    long mPlaylistId = DEFAULT_PLAYLIST;

    private ServiceToken mToken;
    private NavigationView mNavigationView;
    private FragmentManager mFragmentManager;
    private FrameLayout mFrameLayout;
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private TextView volLevel;
    private ImageView volButton;
    private AudioManager mAudioManager;
    private CircularSeekBar mCircularSeekBar;
    private Timer mTimer;
    private int activeTab;

    private Thread mFavoritePlaylistThread = new Thread() {
        @Override
        public void run() {
            if (checkSelfPermission(permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_GRANTED)
                createFavoritePlaylist();
        }
    };

    private ServiceConnection autoShuffle = new ServiceConnection() {
        public void onServiceConnected(ComponentName classname, IBinder obj) {
            // we need to be able to bind again, so unbind
            try {
                unbindService(this);
            } catch (IllegalArgumentException e) {
            }
            IMediaPlaybackService serv = IMediaPlaybackService.Stub
                    .asInterface(obj);
            if (serv != null) {
                try {
                    serv.setShuffleMode(MediaPlaybackService.SHUFFLE_AUTO);
                } catch (RemoteException ex) {
                }
            }
        }

        public void onServiceDisconnected(ComponentName classname) {
        }
    };

    public MusicBrowserActivity() {
    }

    @Override
    public void onCreate(Bundle icicle) {
        if (PermissionActivity.checkAndRequestPermission(this, REQUIRED_PERMISSIONS)) {
            SysApplication.getInstance().exit();
        }

        super.onCreate(icicle);

        if (Aesthetic.isFirstTime()) {
            Aesthetic.get()
                    .activityTheme(R.style.AppTheme)
                    .textColorPrimaryRes(R.color.colorTextPrimary)
                    .textColorSecondaryRes(R.color.colorTextSecondary)
                    .colorPrimaryRes(R.color.colorPrimary)
                    .colorAccentRes(R.color.colorAccent)
                    .colorStatusBarAuto()
                    .colorNavigationBarAuto()
                    .textColorPrimary(Color.BLACK)
                    .navigationViewMode(NavigationViewMode.SELECTED_ACCENT)
                    .apply();
        }

        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setContentView(R.layout.music_browser);

        mIntentAction = getIntent().getAction();
        Bundle bundle = getIntent().getExtras();
        try {
            if (bundle != null) {
                mPlaylistId = Long.parseLong(bundle.getString("playlist"));
            }
        } catch (NumberFormatException e) {
            Log.w(TAG, "Playlist id missing or broken");
        }

        MusicUtils.updateGroupByFolder(this);
        init();
        initView();
        initVolControl();

        mFavoritePlaylistThread.start();
        String shuffle = getIntent().getStringExtra("autoShuffle");
        if ("true".equals(shuffle)) {
            mToken = MusicUtils.bindToService(this, autoShuffle);
        }
    }

    private void initVolControl() {
        try {
            mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            volButton = (ImageView) findViewById(R.id.volButton);
            volLevel = (TextView) findViewById(R.id.volLevel);
            mCircularSeekBar = (CircularSeekBar) findViewById(R.id.seekVolume);
            mTimer = new Timer(false);

            Aesthetic.get()
                    .colorAccent()
                    .take(1)
                    .subscribe(color -> {
                        mCircularSeekBar.setCircleProgressColor(color);
                        mCircularSeekBar.setPointerColor(color);
                    });

            volButton.setOnClickListener(view -> {
                volLevel.setVisibility(View.VISIBLE);
                mCircularSeekBar.setVisibility(View.VISIBLE);
            });

            //Set current level
            volLevel.setText(String.valueOf(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC)));
            mCircularSeekBar.setProgress(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));

            //Set max level
            mCircularSeekBar.setMax(mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));

            mCircularSeekBar.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
                @Override
                public void onProgressChanged(CircularSeekBar circularSeekBar, float progress, boolean fromUser) {
                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int) progress, 0);
                    volLevel.setText(String.valueOf(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC)));
                }

                @Override
                public void onStopTrackingTouch(CircularSeekBar seekBar) {
                    mTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    volLevel.setVisibility(View.INVISIBLE);
                                    mCircularSeekBar.setVisibility(View.INVISIBLE);
                                }
                            });
                        }
                    }, 6000);
                }

                @Override
                public void onStartTrackingTouch(CircularSeekBar seekBar) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createFavoritePlaylist() {
        ContentResolver resolver = getContentResolver();
        int id = MusicUtils.idForplaylist(this, "My Favorite");
        Uri uri;
        if (id < 0) {
            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Audio.Playlists.NAME, "My Favorite");
            try {
                uri = resolver.insert(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, values);
            } catch (UnsupportedOperationException e) {
                Log.w(TAG, "external storage not ready");
            }
        }
    }

    public void initView() {
        mNowPlayingIcon = (ImageView) findViewById(R.id.nowPlay_icon);
        mNowPlayingIcon.setOnClickListener(mPauseListener);

        mToolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(mToolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        mFragmentManager = getFragmentManager();
        mFrameLayout = (FrameLayout) findViewById(R.id.fragment_page);
        mFrameLayout.setVisibility(View.VISIBLE);

        activeTab = MusicUtils.getIntPref(MusicBrowserActivity.this, "activetab", 0);
        showScreen(activeTab);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (getIntent() != null) {
            mArtistID = getIntent().getStringExtra("artist");
            mAlbumID = getIntent().getStringExtra("album");
            Bundle bundle = getIntent().getExtras();
            try {
                if (bundle != null) {
                    mPlaylistId = Long.parseLong(bundle.getString("playlist"));
                    if (mPlaylistId != DEFAULT_PLAYLIST) {
                        if (mIsPanelExpanded) {
                            getSlidingPanelLayout().setHookState(BoardState.COLLAPSED);
                            mIsPanelExpanded = false;
                            try {
                                Thread.sleep(200);  //This is to let the sliding panel collapse and
                                //not to see graphic corruption.
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        }
                    }
                }
            } catch (NumberFormatException e) {
                Log.w(TAG, "Playlist id missing");
            }
            initView();
        }
        mIntentAction = intent.getAction();
    }

    public void showScreen(int position) {
        FragmentManager mFragmentManager = getFragmentManager();
        Fragment mFragment = null;

        if (MusicUtils.isGroupByFolder()) {
            mToolbar.setTitle(getResources().getStringArray(
                    R.array.title_array_folder)[position]);
        } else {
            mToolbar.setTitle(getResources().getStringArray(
                    R.array.title_array_songs)[position]);
        }

        MusicUtils.setIntPref(this, "activetab", position);
        if (mArtistID != null) {
            mFragment = new AlbumBrowserFragment();
            Bundle bundle = new Bundle();
            bundle.putString("artist", mArtistID);
            mFragment.setArguments(bundle);
            mArtistID = null;
        } else if (mAlbumID != null) {
            mFragment = new TrackBrowserFragment();
            Bundle bundle = new Bundle();
            bundle.putString("album", mAlbumID);
            bundle.putBoolean("editValue", false);
            mFragment.setArguments(bundle);
            mAlbumID = null;
        } else if (mPlaylistId != DEFAULT_PLAYLIST) {
            mFragment = new TrackBrowserFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("editValue", false);
            if (mPlaylistId == RECENTLY_ADDED_PLAYLIST) {
                bundle.putString("playlist", "recentlyadded");
            } else if (mPlaylistId == PODCASTS_PLAYLIST) {
                bundle.putString("playlist", "podcasts");
            } else {
                if(mPlaylistId != ALL_SONGS_PLAYLIST) {
                    bundle.putBoolean("editValue", true);
                    bundle.putString("playlist", Long.valueOf(mPlaylistId)
                            .toString());
                }
            }
            bundle.putBoolean("isFromShortcut", true);
            mFragment.setArguments(bundle);
            mPlaylistId = DEFAULT_PLAYLIST;
        } else if (mIntentAction != null
                && mIntentAction
                .equalsIgnoreCase(Intent.ACTION_CREATE_SHORTCUT)) {
            mFragment = new PlaylistBrowserFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isFromShortcut", true);
            mFragment.setArguments(bundle);
        }
        else {
            mFragment = FragmentsFactory.loadFragment(position);
        }
        if (!mFragment.isAdded()) {
            mFragmentManager.beginTransaction().replace(R.id.fragment_page, mFragment).commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (mToken != null) {
            MusicUtils.unbindFromService(mToken);
        }
        MusicUtils.clearAlbumArtCache();
        super.onDestroy();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_track:
                mFragmentManager.beginTransaction().replace(R.id.fragment_page, new TrackBrowserFragment()).commit();
                break;
            case R.id.action_album:
                mFragmentManager.beginTransaction().replace(R.id.fragment_page, new AlbumBrowserFragment()).commit();
                break;
            case R.id.action_artist:
                mFragmentManager.beginTransaction().replace(R.id.fragment_page, new ArtistAlbumBrowserFragment()).commit();
                break;
            case R.id.action_playlist:
                mFragmentManager.beginTransaction().replace(R.id.fragment_page, new PlaylistBrowserFragment()).commit();
                break;
            case R.id.action_folder:
                mFragmentManager.beginTransaction().replace(R.id.fragment_page, new FolderBrowserFragment()).commit();
                break;
            case R.id.action_themes:
                if (mIsPanelExpanded) {
                    getSlidingPanelLayout().setHookState(BoardState.COLLAPSED);
                    mIsPanelExpanded = false;
                }
                mFragmentManager.beginTransaction().replace(R.id.fragment_page, new ThemesActivityFragment()).commit();
                break;
            case R.id.action_equalizer:
                MusicUtils.startSoundEffectActivity(MusicBrowserActivity.this);
                break;
            case R.id.action_exit:
                try {
                    MusicUtils.sService.stop();
                    SysApplication.getInstance().exit();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onColorSelection(@NonNull ColorChooserDialog dialog, int selectedColor) {
        if (dialog.isAccentMode()) {
            Aesthetic.get().colorAccent(selectedColor);
        } else {
            Aesthetic.get()
                    .colorPrimary(selectedColor)
                    .colorStatusBarAuto()
                    .colorNavigationBarAuto()
                    .navigationViewMode(NavigationViewMode.SELECTED_ACCENT)
                    .apply();
        }
    }

    @Override
    public void onColorChooserDismissed(@NonNull ColorChooserDialog dialog) {

    }

    public static class SimplePanelSlideListener implements
            ViewHookSlipListener {

        @Override
        public void onViewSlip(View view, float slipOffset) {
            mIsParentActivityFinishing = true;
        }

        @Override
        public void onViewClosed(View view) {
            getInstance().mIsPanelExpanded = false;
            MusicUtils.updateNowPlaying(getInstance(), false);
            getInstance().closeQueuePanel();
        }

        @Override
        public void onViewOpened(View view) {
            getInstance().mIsPanelExpanded = true;
            MusicUtils.updateNowPlaying(getInstance(), true);
        }

        @Override
        public void onViewMainLined(View panel) {
        }

        @Override
        public void onViewBackStacked(View panel) {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Just a work around
        mNowPlayingView.setVisibility(View.GONE);
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                Intent intent = new Intent(MusicBrowserActivity.this,
                        QueryBrowserActivity.class);
                startActivity(intent);
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    public void applyAccent(ImageView myImageView) {
        Aesthetic.get()
                .colorAccent()
                .take(1)
                .subscribe(myImageView::setColorFilter);
    }
}
